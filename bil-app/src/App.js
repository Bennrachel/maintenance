import React from 'react'
import { Grid } from '@material-ui/core'
import NavBar from './NavBar'
import './index.css';

export default function App() {
  return (
    <Grid container direction="row">
      <NavBar />
      <div className="pageContainer">
        <Grid className="spaceNav"></Grid>
        <div className="pageWrapper">
          <img src="/maintenance.svg" alt="Maintenance" width="400px" />
          <div className="pageText">
            BuyItLive is down for planned maintenance.
            We're working on some updates, but we should be back up and running within a few hours. <br /><br />
            <b>Visit our <a href="https://www.facebook.com/buyitlive">Facebook page</a> to get updates.</b>
          </div>
        </div>
      </div>
    </Grid>
  );
}
