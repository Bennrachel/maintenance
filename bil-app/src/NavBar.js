import React from 'react'
import { AppBar } from '@material-ui/core'
import './index.css';

export default function NavBar() {

    return (
        <>
            <AppBar position='fixed' className="appbar">
                <div className="spaceAround">
                    <div className="dflex">
                        <img src='/buyitlive-logo-full.svg' alt="Navigation" />
                    </div>
                </div>
            </AppBar>
        </>
    )
}